package driver;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import utils.PropertyReader;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public class DriverManager {
    PropertyReader propertyReader = new PropertyReader();
    private AndroidDriver<AndroidElement> driver;

    public AndroidDriver<AndroidElement> driverInitialize() throws IOException {

        File filePathName = new File(propertyReader.getGlobalValue("filePath"));
        File fileName = new File(filePathName, propertyReader.getGlobalValue("fileName"));

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, propertyReader.getGlobalValue("DEVICE_NAME"));
        capabilities.setCapability(MobileCapabilityType.APP, fileName.getAbsolutePath());
        capabilities.setCapability("appPackage" , propertyReader.getGlobalValue("PACKAGE"));
        capabilities.setCapability("appActivity" , propertyReader.getGlobalValue("ACTIVITY"));

        driver = new AndroidDriver<AndroidElement>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
        return driver;

    }

    public void closeDriver(){
        driver.quit();
    }
}
