package pages;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SignInPage extends BasePage{

    public SignInPage(AndroidDriver<AndroidElement> driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "com.cleartrip.android:id/username_field")
    protected WebElement emailField;


    @FindBy(id = "com.cleartrip.android:id/password_field")
    protected WebElement passwordField;

    @FindBy(id = "com.cleartrip.android:id/signin_button")
    protected WebElement signInButton;

    @FindBy(xpath = "//android.widget.TextView[@text='REGISTER']")
    protected WebElement naviagateToRegister;

    @FindBy(id = "com.cleartrip.android:id/achf_user_email")
    protected WebElement userEmail;

    @FindBy(xpath = "//android.widget.TextView[@text='Sign out']")
    protected WebElement signOut;



    public SignInPage enterUserName(String userName){
        sendKeys(emailField , userName);
        return new SignInPage(driver);
    }

    public SignInPage enterPassword(String password){
        sendKeys(passwordField , password);
        return new SignInPage(driver);
    }

    public void signIn(){
        clickOn(signInButton);
    }

    public boolean isUserInLoginInScreen(){
        staticWait();
        return signInButton.isDisplayed();
    }

    public void clearUserNameAndPassword(){
        passwordField.clear();
        emailField.clear();
    }

    public String getEmailIdOfLoggedUser(){
        return getText(userEmail);
    }

    public void logOut(){

        scrollToText("Sign out");
        clickOn(signOut);
    }


    public RegisterPage clickOnRegisterButton(){
        clickOn(naviagateToRegister);
        return new RegisterPage(driver);
    }
}
