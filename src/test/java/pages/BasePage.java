package pages;

import io.appium.java_client.MobileBy;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;

import java.time.Duration;

public class BasePage {

    protected AndroidDriver driver;
    private WebDriverWait wait;
    private FluentWait<WebDriver> fluentlyWait;

    public BasePage(AndroidDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(this.driver, 40);
        fluentlyWait = new FluentWait<WebDriver>(this.driver)
                .withTimeout(Duration.ofSeconds(40))
                .pollingEvery(Duration.ofSeconds(1))
                .ignoring(StaleElementReferenceException.class);
    }
    protected void waitForElementToBeClickable(WebElement element) {
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    protected String getText(WebElement webElement) {
        waitForElementToBeVisible(webElement);
        return webElement.getText();
    }

    protected WebElement waitForElementToBeVisible(WebElement element) {
        return wait.until(ExpectedConditions.visibilityOf(element));
    }

    protected void clickOn(WebElement webElement) {
        waitForElementToBeClickable(webElement);
        webElement.click();
    }

    protected void staticWait() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            Reporter.log("Exception in Static wait method while publishing post");
        }
    }

    protected void sendKeys(WebElement elem, String text) {
        waitForElementToBeClickable(elem);
        elem.click();
        if (text != null) {
            if (!elem.getText().isEmpty()) {
                elem.clear();
            }
            elem.sendKeys(text);
        }
    }


    protected WebElement scrollToText(String text) {
        try {
            return driver.findElement(MobileBy.AndroidUIAutomator(
                    "new UiScrollable(new UiSelector()" +
                            ".scrollable(true)" +
                            ".instance(0))" +
                            ".scrollIntoView(new UiSelector()" +
                            ".text(\"" + text + "\")" +
                            ".instance(0))"
            ));
        } catch (Exception e) {
            Assert.fail("Did not find element by text - " + text);
        }
        return null;
    }

    protected void hideKeyboard() {
        try {
            driver.hideKeyboard();
        } catch (WebDriverException e) {
            // ignore exception
        }
    }

}
