package pages;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RegisterPage extends BasePage {
    public RegisterPage(AndroidDriver<AndroidElement> driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id = "com.cleartrip.android:id/signup_fname")
    protected WebElement firstNameButton;

    @FindBy(id = "com.cleartrip.android:id/signup_lname")
    protected WebElement lastNameButton;

    @FindBy(id = "com.cleartrip.android:id/signup_email")
    protected WebElement emailButton;

    @FindBy(id = "com.cleartrip.android:id/signup_phNum")
    protected WebElement mobileNumberButton;

    @FindBy(id = "com.cleartrip.android:id/signup_pwd")
    protected WebElement passwordButton;

    @FindBy(id = "com.cleartrip.android:id/signup_register_button")
    protected WebElement registerButton;

    @FindBy(id = "com.cleartrip.android:id/tv_password_error_register")
    protected WebElement passwordError;


    @FindBy(id = "com.cleartrip.android:id/cb_promotional_mails")
    protected WebElement subscribeToOffers;

    public RegisterPage enterFirstNameAndLastName(String firstName , String lastName){
        sendKeys( firstNameButton , firstName);
        sendKeys( lastNameButton , lastName);
        return new RegisterPage(driver);
    }

    public RegisterPage enterEmailID(String email){
        sendKeys( emailButton , email);
        return new RegisterPage(driver);
    }

    public RegisterPage enterMobileNumber(String mobile){
        sendKeys( mobileNumberButton , mobile);
        return new RegisterPage(driver);
    }

    public RegisterPage enterPassword(String password){
        sendKeys( passwordButton , password);
        hideKeyboard();
        return new RegisterPage(driver);
    }

    public boolean isSubscribeClicked(){
        return  subscribeToOffers.isSelected();
    }

    public void clickOnSubscribe(){
        clickOn(subscribeToOffers);
    }

    public String getPasswordErrorMessage(){
        return getText(passwordError);
    }

    public RegisterPage clearPassword(){
        passwordButton.clear();
        return new RegisterPage(driver);
    }


    public RegisterPage clickOnRegisterButton(){
        hideKeyboard();
        clickOn(registerButton);
        return new RegisterPage(driver);
    }













}
