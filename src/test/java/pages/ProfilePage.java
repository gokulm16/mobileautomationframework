package pages;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ProfilePage extends BasePage {
    public ProfilePage(AndroidDriver<AndroidElement> driver) {
        super(driver);
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }
    @FindBy(xpath = "//android.widget.TextView[@text='You']")
    protected WebElement navigateToProfilePageButton;

    @FindBy(id = "com.cleartrip.android:id/signin_btn")
    protected WebElement signInButton;





    public ProfilePage navigateToProfileScreen(){
        clickOn(navigateToProfilePageButton);
        return new ProfilePage(driver);
    }

    public SignInPage clickOnSignInButton(){
        clickOn(signInButton);
        return new SignInPage(driver);
    }

    public boolean isSignInButtonVisible(){
       return signInButton.isDisplayed();
    }




}
