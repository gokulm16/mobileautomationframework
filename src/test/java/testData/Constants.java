package testData;

public final class  Constants {
    public static final String incorrectUserName = "cleartrip@gmail.com";
    public static final String incorrectPassword = "ClearTrip123";

    public static final String userName = "testingprofilemytestprofile@gmail.com";
    public static final String password = "ClearTrip@123456";

    public static final String extendReportPath= System.getProperty("user.dir")+"/ExtentReports/index.html";

    public static final String firstName = "John";
    public static final String lastName = "Doe";
    public static final String mobileNumber = "1234567652";

    public static final String passwordFormatError = "Password must contain an minimum 8 characters";
    public static final String passwordError = "Password can't be empty. ";

    public static final String smallPassword = "@1gT";
}
