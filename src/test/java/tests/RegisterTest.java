package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.ProfilePage;
import pages.RegisterPage;
import testData.Constants;

public class RegisterTest extends TestBase {

    RegisterTest(){
        profilePage = new ProfilePage(driver);
        registerPage = new RegisterPage(driver);
    }

    @Test(priority = 1)
    public void ValidateEmptyPasswordForRegister() {
        profilePage.navigateToProfileScreen().clickOnSignInButton()
                .clickOnRegisterButton().enterEmailID(Constants.userName)
                .enterFirstNameAndLastName(Constants.firstName, Constants.lastName)
                .enterMobileNumber(Constants.mobileNumber).clickOnRegisterButton();

        Assert.assertEquals(registerPage.getPasswordErrorMessage(), Constants.passwordError);

    }

    @Test(priority = 2)
    public void ValidateLengthOfPasswordForRegister(){
        registerPage = new RegisterPage(driver);
        registerPage.clearPassword()
                .enterPassword(Constants.smallPassword)
                .clickOnRegisterButton();

        Assert.assertEquals(registerPage.getPasswordErrorMessage(), Constants.passwordFormatError);

    }

}
