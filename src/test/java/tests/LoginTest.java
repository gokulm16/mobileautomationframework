package tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import pages.ProfilePage;
import pages.SignInPage;
import testData.Constants;

public class LoginTest extends TestBase {

    LoginTest(){
        profilePage = new ProfilePage(driver);
        signInPage = new SignInPage(driver);

    }

    @Test(priority = 0)
    public void loginTestWithInvalidUserNameAndPassword()  {
        profilePage.navigateToProfileScreen()
                .clickOnSignInButton()
                .enterUserName(Constants.incorrectUserName).enterPassword(Constants.incorrectPassword)
                .signIn();
        Assert.assertEquals(signInPage.isUserInLoginInScreen() , true);
        signInPage.clearUserNameAndPassword();


    }

    @Test(priority = 1)
    public void LoginTestWithEmptyUserName()  {
        signInPage.enterPassword(Constants.incorrectPassword)
                .signIn();
        Assert.assertEquals(signInPage.isUserInLoginInScreen() , true);
        signInPage.clearUserNameAndPassword();
    }

    @Test(priority = 2)
    public void LoginTestWithEmptyPassword()  {
        signInPage.enterUserName(Constants.incorrectUserName)
                .signIn();
        Assert.assertEquals(signInPage.isUserInLoginInScreen() , true);
        signInPage.clearUserNameAndPassword();
    }

    @Test(priority = 3)
    public void LoginTestWithCorrectUserNameAndPassword()  {
        signInPage.enterUserName(Constants.userName).enterPassword(Constants.password)
                .signIn();

        Assert.assertEquals(signInPage.getEmailIdOfLoggedUser() , Constants.userName);
    }

    @Test(priority = 4)
    public void LogOutTest()  {
        signInPage.logOut();
        Assert.assertEquals(profilePage.isSignInButtonVisible() , true);
    }

}
