package tests;

import driver.DriverManager;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import pages.ProfilePage;
import pages.RegisterPage;
import pages.SignInPage;
import utils.PropertyReader;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

public class TestBase {
    SignInPage signInPage ;
    ProfilePage profilePage;
    RegisterPage registerPage;
    DriverManager driverManager = new DriverManager();

    @BeforeTest(alwaysRun = true)
    public AndroidDriver<AndroidElement> driverInitialize() throws IOException {
        return driverManager.driverInitialize();

    }

    @AfterTest(alwaysRun = true)
    public void tearDownMethod(){
        driverManager.closeDriver();
    }



}
